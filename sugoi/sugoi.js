$(function(){
  var url = 'http://sugoiweb.nezihiko.com',
  shareText = 'すごいWEB（たおちゃんが勉学のために作成しました）下：参考URL';

/* ウィンドウ開くやつの定義 */
  function openWin(url, id){
    window.open(url, id, 'width=550, height=450,personalbar=0,toolbar=0,scrollbars=1,resizable=1');
  }
/* ここまで */

/* TwitterのURL */
  function getTwitterUrl() {
    var text = 'http://twitter.com/share?';
    text += 'url=' + encodeURIComponent(url) + '&';
    text += 'text=' + shareText;
    return text;
  }
/* ここまで */

/* ヘッダーとフッターのTwitterとFacebookに飛ばすやつ */
  $('.share-btn').click(function(){
    openWin('http://www.facebook.com/share.php?u=' + url, 'facebook_share');
  });
  $('.tweet-btn').click(function(){
    openWin(getTwitterUrl(), 'twitter_share');
  });
/* ここまで */

/* スライドさせるやつ（結構長いやん）*/
  var initialScrollleft = ($('#slideshow li').width()*1.5 - $(window).width()/2);
  $('#slideshow').scrollLeft(initialScrollleft);

  var elemFirst;
  var domFirst;

  setInterval(
    function(){
      $('#slideshow').animate(
        {
          scrollLeft: (initialScrollleft + $('#slideshow li').width()) + 'px'
        },
        1000,
        'easeInOutQuart',
        function(){
          elemFirst = $('#slideshow li').eq(0);
          domFirst = '<li>' + elemFirst.html() + '</li>';
          elemFirst.remove();
          $('#slideshow ul').append(domFirst);
          $('#slideshow').scrollLeft(initialScrollleft);
        }
      );
    },
  3000);
/* ここまで */

/* movie見せるやつ */
  $('#movieplay').click(function(){
    $(this).parent().children().remove();
    $('#movieshow').prepend('<iframe width="640" height="460" src="http://www.youtube.com/embed/BXrjXna-_MA?rel=0&autoplay=1" frameborder="0"></iframe>');
  });
/* ここまで */

/* ボタンのモーション */
  $('.motion-btn').mouseout(function(){
    $(this).css('background-position-y',(0 * $(this).height()) + 'px');
  });
  $('.motion-btn').mouseover(function(){
    $(this).css('background-position-y',((-1) * $(this).height()) + 'px');
  });
  $('.motion-btn').mouseup(function(){
    $(this).css('background-position-y',((-1) * $(this).height()) + 'px');
  });
  $('.motion-btn').mousedown(function(){
    $(this).css('background-position-y',((-2) * $(this).height()) + 'px');
  });
/* ここまで */
});
