$(function() {
  $('header a').click(function() {
    var $place = $(this).attr('href');
    var $position = $($place).offset().top;
    $('html, body').animate({
      'scrollTop' : $position
    }, 500);
  });
  
  $('#read-more').click(function() {
    var $read = $('.more-text');
    if ($read.hasClass('open')) {
      $read.removeClass('open');
      $('#more-text').slideUp();
      $(this).text('もっと詳しく');
    } else {
      $read.addClass('open');
      $('#more-text').slideDown();
      $(this).text('閉じる');
    }
  });
  
  $('.languages-box').hover(function() {
    $(this).find('p').fadeIn();
  },function() {
    $(this).find('p').fadeOut();
  });
  
  $('.btn-pricing').click(function() {
    $('#apply-modal').fadeIn();
    var $courseString = $(this).parents('.price-column').find('h2').text();
    $('#apply-form').find('h2').text('【' + $courseString + '】' + 'に申し込む');
    var $course = $(this).attr('id');
    $('#course-select').val($course);
  });
  
  $('#close-modal').click(function() {
    $('#apply-modal').fadeOut();
  });
  
  $('.column-image').hover(
    function(){
      $(this).addClass('zoom');
      $(this).children('.zoom-black').fadeIn(300);
    },
    function(){
      $(this).removeClass('zoom');
      $(this).children('.zoom-black').fadeOut(300);
    }
  );
  
  $('.filter-item').click(function(){
    $('.filter-item').removeClass('active');
    $(this).addClass('active');
    var category = $(this).attr('id');
    if (category === 'all') {
      $('.column-box').fadeIn();
    } else {
      $('.column-box').hide();
      $('.column-box-wrapper').children('.' + category).fadeIn();
    }
  });
  
  $('.contact-form form').submit(function(){
    var error_flag = false;
    $(this).children('input, textarea').each(function(){
      var body = $(this).val();
      if (!body) {
        $(this).prev('.error-message').text('入力してください');
        error_flag = true;
      } else {
        $(this).prev('.error-message').text('');
      }
    });
    if (!error_flag) {
      $('.contact-form').html('<h4>お問い合わせありがとうございます。</h4>');
    }
    return false;
  });
  
  $('.carousel').carousel({
    interval: 2500
  });
});